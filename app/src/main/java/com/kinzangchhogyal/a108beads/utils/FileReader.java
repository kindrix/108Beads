package com.kinzangchhogyal.a108beads.utils;

import android.content.Context;
import android.util.Log;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.models.Mantra;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 * Created by kinzang on 15/03/2017.
 */

public class FileReader {

    private Context mContext;
    public FileReader(Context context){
        this.mContext = context;
    }

    public  ArrayList<Mantra> getMantraList(){
        //file containing mantras
        int dataFile = R.raw.mantra_data;

        SharedPreferencesHelper sharedPreferencesHelper = new SharedPreferencesHelper(mContext);

        //array list to store mantras
        ArrayList<Mantra> listMantra = new ArrayList<>();

        //open stream
        InputStream inputStream= mContext.getResources().openRawResource(R.raw.mantra_data);
        try {
            BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));
            String line;
            while ((line = bufferedReader.readLine()) != null){
                String[] rowData = line.split(":");

                //Note image uri is not contained in data file. It is the same as the name but all lowercase.
                //Check Mantra model class to see what i mean.
                //last three 0s for mantra counts
                Mantra mantra = new Mantra(rowData[0], rowData[1], rowData[2], rowData[3], rowData[0].toLowerCase(), rowData[4], 0, 0, 0);
                listMantra.add(mantra);

            }
        } catch (UnsupportedEncodingException e) {
            Log.d(this.getClass() + "", "UTF-8 Encoding Unsupported");
        }
        catch (IOException e){
            Log.d(this.getClass() + "", "IO Exception");
        } finally {
            try {
                inputStream.close();
            }
            catch (IOException e){

            }
        }
        return listMantra;
    }
}
