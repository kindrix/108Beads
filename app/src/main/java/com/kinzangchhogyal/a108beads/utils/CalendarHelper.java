package com.kinzangchhogyal.a108beads.utils;

import android.content.Context;
import android.util.Log;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.models.Mantra;

import java.util.Calendar;
import java.util.Locale;

/**
 * Created by kinzang on 29/03/2017.
 */

public class CalendarHelper {


    //save installation date for app, this is useful for calculating some stats
    public static void setInstallationDate(Context context, SharedPreferencesHelper sharedPreferencesHelper){
        Calendar calendar = Calendar.getInstance();
        //for testing
//        sharedPreferencesHelper.putInt(context.getString(R.string.prefs_app_install_year), 2017);
//        sharedPreferencesHelper.putInt(context.getString(R.string.prefs_app_install_month), 3);
//        sharedPreferencesHelper.putInt(context.getString(R.string.prefs_app_install_day_of_month), 13);


        sharedPreferencesHelper.putInt(context.getString(R.string.prefs_app_install_year), calendar.get(Calendar.YEAR));
        sharedPreferencesHelper.putInt(context.getString(R.string.prefs_app_install_month), calendar.get(Calendar.MONTH));
        sharedPreferencesHelper.putInt(context.getString(R.string.prefs_app_install_day_of_month), calendar.get(Calendar.DAY_OF_MONTH));
    }

    //get calendar get installation date
    public static Calendar getInstallationDate(Context context, SharedPreferencesHelper sharedPreferencesHelper){
        Calendar calendar = Calendar.getInstance();
        calendar.set(Calendar.YEAR, sharedPreferencesHelper.getInt(context.getString(R.string.prefs_app_install_year)));
        calendar.set(Calendar.MONTH, sharedPreferencesHelper.getInt(context.getString(R.string.prefs_app_install_month)));
        calendar.set(Calendar.DAY_OF_MONTH, sharedPreferencesHelper.getInt(context.getString(R.string.prefs_app_install_day_of_month)));

        return calendar;
    }

    //difference between two calendar dates, useful for stats
    public static int getDiffBetweenTwoCalendars(Calendar start, Calendar end){
        //make sure time for both calendars are set to midnight and millisecond offset is 0
        start.set(Calendar.HOUR_OF_DAY, 0);
        start.set(Calendar.MINUTE, 0);
        start.set(Calendar.SECOND, 0);
        start.set(Calendar.MILLISECOND, 0);

        end.set(Calendar.HOUR_OF_DAY, 0);
        end.set(Calendar.MINUTE, 0);
        end.set(Calendar.SECOND, 0);
        end.set(Calendar.MILLISECOND, 0);

        //convert date to milliseconds since epoch
        Long diff = end.getTimeInMillis() - start.getTimeInMillis() ;

        //divid by milliseconds in a year
        int days = (int) (diff / (24 * 60 * 60 * 1000));

        return Math.abs(days);
    }


    // Set day of year app last used
    public static void setUpdateDayAppLastUsed(Context context, SharedPreferencesHelper sharedPreferencesHelper){
        //for test , uncomment second one for production
        //sharedPreferencesHelper.putInt(context.getString(R.string.prefs_day_of_year_app_last_used), Calendar.getInstance().get(Calendar.HOUR_OF_DAY));

        sharedPreferencesHelper.putInt(context.getString(R.string.prefs_day_of_year_app_last_used), Calendar.getInstance().get(Calendar.DAY_OF_YEAR));
    }

    // Get  day of year app last used, useful for stats
    public static int getDayAppLastUsed(Context context, SharedPreferencesHelper sharedPreferencesHelper){
        return sharedPreferencesHelper.getInt(context.getString(R.string.prefs_day_of_year_app_last_used));
    }

    //Get current day of year
    public static int getCurrentDayOfYear(){
        //for test uncomment second one
        //Log.d("Current hour", Calendar.getInstance().get(Calendar.HOUR_OF_DAY)+"");
        //return Calendar.getInstance().get(Calendar.HOUR_OF_DAY);

        return Calendar.getInstance().get(Calendar.DAY_OF_YEAR);
    }

    //checks if date has changed - update view accordingly
    public static boolean checkIfDateChanged(Context context, SharedPreferencesHelper sharedPreferencesHelper){
        if (getCurrentDayOfYear() != sharedPreferencesHelper.getInt(context.getString(R.string.prefs_day_of_year_app_last_used)))
            return true;
        return false;
    }



}
