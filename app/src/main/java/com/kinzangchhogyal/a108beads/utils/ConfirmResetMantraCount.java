package com.kinzangchhogyal.a108beads.utils;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;

import com.kinzangchhogyal.a108beads.R;

/**
 * Created by kinzang on 20/03/2017.
 */

public class ConfirmResetMantraCount extends DialogFragment {

    private ResetMantraCountInterface mListener;

    //factory method
    public static ConfirmResetMantraCount newInstance(int num) {
        ConfirmResetMantraCount f = new ConfirmResetMantraCount();

        // Supply num input as an argument.
        Bundle args = new Bundle();
        args.putInt("num", num);
        f.setArguments(args);

        return f;
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        int mNum = getArguments().getInt("num");

        String message = String.format("%s %d. %s",getString(R.string.dialog_message_reset_mantra_count_1),mNum, getString(R.string.dialog_message_reset_mantra_count_2));
        //String message = R.string.dialog_message_reset_mantra_count_1; //+ mNum +". " + R.string.dialog_message_reset_mantra_count_2;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setMessage(message)
                .setTitle(R.string.dialog_title_reset_mantra_count);
        builder.setPositiveButton(R.string.dialog_yes_reset_mantra_count, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                mListener.onChoose();
            }
        })
        .setNegativeButton(R.string.dialog_cancel_reset_mantra_count, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
                // User cancelled the dialog
            }
        });
        return  builder.create();
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (ResetMantraCountInterface) context;
    }


}
