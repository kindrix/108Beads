package com.kinzangchhogyal.a108beads.utils;

import android.content.Context;
import android.widget.Toast;

/**
 * Created by kinzang on 28/03/2017.
 */

public class ToastHelper {
    public static void showToastShort(Context context, String message){
        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_SHORT;
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
    public static void showToastLong(Context context, String message){
        CharSequence text = "Hello toast!";
        int duration = Toast.LENGTH_LONG;
        Toast toast = Toast.makeText(context, message, duration);
        toast.show();
    }
}
