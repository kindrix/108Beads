package com.kinzangchhogyal.a108beads.utils;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.provider.BaseColumns;
import android.util.Log;

import com.kinzangchhogyal.a108beads.models.Mantra;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kinzang on 6/04/2017.
 */

public class DBHandler extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "mantra.db";

    //database table and column constants
    public static class MantraDBConstants implements BaseColumns{
        public static final String TABLE_NAME = "mantra";
        public static final String COLUMN_MANTRA_NAME = "mantra_name"; //mantra name
        public static final String COLUMN_MANTRA_NAME_TIBETAN = "mantra_name_tibetan"; //Tibetan Unicode name
        public static final String COLUMN_MANTRA_WORDS = "mantra_words"; //mantra words
        public static final String COLUMN_MANTRA_WORDS_TIBETAN="mantra_words_tibetan"; //Tibetan unicode mantra words
        public static final String COLUMN_MANTRA_DESCRIPTION="mantra_description"; //mantra desc
        public static final String COLUMN_MANTRA_IMAGE_URI="mantra_image_uri"; //image resource for detail view of mantra
        public static final String COLUMN_MANTRA_DAILY_COUNT="mantra_daily_count";
        public static final String COLUMN_MANTRA_TOTAL_COUNT="mantra_total_count";
        public static final String COLUMN_MANTRA_108_COUNT="mantra_108_count";
    }

    //create table and columsn string

    String SQL_CREATE_ENTRIES = "CREATE TABLE " + MantraDBConstants.TABLE_NAME + "("
            + MantraDBConstants._ID + " INT PRIMARY KEY," +
            MantraDBConstants.COLUMN_MANTRA_NAME + " TEXT," +
            MantraDBConstants.COLUMN_MANTRA_NAME_TIBETAN + " TEXT," +
            MantraDBConstants.COLUMN_MANTRA_WORDS + " TEXT," +
            MantraDBConstants.COLUMN_MANTRA_WORDS_TIBETAN + " TEXT," +
            MantraDBConstants.COLUMN_MANTRA_IMAGE_URI + " TEXT," +
            MantraDBConstants.COLUMN_MANTRA_DESCRIPTION + " TEXT," +
            MantraDBConstants.COLUMN_MANTRA_108_COUNT + " INT," +
            MantraDBConstants.COLUMN_MANTRA_DAILY_COUNT + " INT," +
            MantraDBConstants.COLUMN_MANTRA_TOTAL_COUNT + " INT" +
            ")";

    //delete table string
    String SQL_DELETE_ENTRIES =
            "DROP TABLE IF EXISTS " + MantraDBConstants.TABLE_NAME;

    //constructor - creates databse
    public DBHandler(Context context){
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /*
    * SQLiteOpenHelper methods
    */

    // Creating Tables
    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(SQL_CREATE_ENTRIES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over

        //TODO - may need to copy data before deleting

        db.execSQL(SQL_DELETE_ENTRIES);
        onCreate(db);
    }

    /*
    * DATABASE OPERATIONS
     */



    // Adding new mantra
    public void addMantra(Mantra m) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MantraDBConstants.COLUMN_MANTRA_NAME,  m.getmName());
        values.put(MantraDBConstants.COLUMN_MANTRA_NAME_TIBETAN, m.getmNameTibetan());
        values.put(MantraDBConstants.COLUMN_MANTRA_WORDS, m.getmWords());
        values.put(MantraDBConstants.COLUMN_MANTRA_WORDS_TIBETAN, m.getmWordsTibetan());
        values.put(MantraDBConstants.COLUMN_MANTRA_IMAGE_URI, m.getmImageUri());
        values.put(MantraDBConstants.COLUMN_MANTRA_DESCRIPTION, m.getmDesc());
        values.put(MantraDBConstants.COLUMN_MANTRA_108_COUNT, m.getm108Count());
        values.put(MantraDBConstants.COLUMN_MANTRA_DAILY_COUNT, m.getmDailyCount());
        values.put(MantraDBConstants.COLUMN_MANTRA_TOTAL_COUNT, m.getmTotalCount());

        // Inserting Row
        db.insert(MantraDBConstants.TABLE_NAME, null, values);
    }





    // Getting single mantra
    public Mantra getMantra(String mantraName) {
        SQLiteDatabase db = this.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                MantraDBConstants.COLUMN_MANTRA_NAME,
                MantraDBConstants.COLUMN_MANTRA_NAME_TIBETAN,
                MantraDBConstants.COLUMN_MANTRA_WORDS,
                MantraDBConstants.COLUMN_MANTRA_WORDS_TIBETAN,
                MantraDBConstants.COLUMN_MANTRA_IMAGE_URI,
                MantraDBConstants.COLUMN_MANTRA_DESCRIPTION,
                MantraDBConstants.COLUMN_MANTRA_108_COUNT,
                MantraDBConstants.COLUMN_MANTRA_DAILY_COUNT,
                MantraDBConstants.COLUMN_MANTRA_TOTAL_COUNT
        };

        // Filter results WHERE "name" = mantraName
        String selection = MantraDBConstants.COLUMN_MANTRA_NAME + " = ?";
        String[] selectionArgs = { mantraName };

        Cursor cursor = db.query(
                MantraDBConstants.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                selection,                                // The columns for the WHERE clause
                selectionArgs,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        if (cursor != null)
            cursor.moveToFirst();
        Mantra m = new Mantra();
        create_mantra_object(cursor, m);

        cursor.close();
        return m;
    }


    //Get all mantras
    public List<Mantra> getAllMantras() {
        List<Mantra> mantraList = new ArrayList<Mantra>();


        SQLiteDatabase db = this.getReadableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                MantraDBConstants.COLUMN_MANTRA_NAME,
                MantraDBConstants.COLUMN_MANTRA_NAME_TIBETAN,
                MantraDBConstants.COLUMN_MANTRA_WORDS,
                MantraDBConstants.COLUMN_MANTRA_WORDS_TIBETAN,
                MantraDBConstants.COLUMN_MANTRA_IMAGE_URI,
                MantraDBConstants.COLUMN_MANTRA_DESCRIPTION,
                MantraDBConstants.COLUMN_MANTRA_108_COUNT,
                MantraDBConstants.COLUMN_MANTRA_DAILY_COUNT,
                MantraDBConstants.COLUMN_MANTRA_TOTAL_COUNT
        };


        Cursor cursor = db.query(
                MantraDBConstants.TABLE_NAME,                     // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                Mantra m = new Mantra();
                create_mantra_object(cursor, m);
                mantraList.add(m);
            } while (cursor.moveToNext());
        }
        cursor.close();

        // return contact list
        return mantraList;
    }



    // Deleting single mantra
    public void deleteContact(String mantraName) {
        SQLiteDatabase db = this.getWritableDatabase();

        // Define 'where' part of query.
        String selection = MantraDBConstants.COLUMN_MANTRA_NAME + " LIKE ?";

        // Specify arguments in placeholder order.
        String[] selectionArgs = { mantraName };

        // Issue SQL statement.
        db.delete(MantraDBConstants.TABLE_NAME, selection, selectionArgs);
    }


    // Updating single mantra
    public int updateMantraCount(Mantra m) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(MantraDBConstants.COLUMN_MANTRA_108_COUNT, m.getm108Count());
        values.put(MantraDBConstants.COLUMN_MANTRA_DAILY_COUNT, m.getmDailyCount());
        values.put(MantraDBConstants.COLUMN_MANTRA_TOTAL_COUNT, m.getmTotalCount());

        // updating row
        return db.update(MantraDBConstants.TABLE_NAME, values, MantraDBConstants.COLUMN_MANTRA_NAME+ " = ?",
                new String[] { m.getmName() });
    }


    //create mantra object from cursor
    private void create_mantra_object(Cursor cursor, Mantra m){
        //Note: we don't start at 0 because 0 is for ID which we do not use
        m.setmName(cursor.getString(0));
        m.setmNameTibetan(cursor.getString(1));
        m.setmWords(cursor.getString(2));
        m.setmWordsTibetan(cursor.getString(3));
        m.setmImageUri(cursor.getString(4));
        m.setmDesc(cursor.getString(5));
        m.setm108Count(Integer.parseInt(cursor.getString(6)));
        m.setmDailyCount(Integer.parseInt(cursor.getString(7)));
        m.setmTotalCount(Integer.parseInt(cursor.getString(8)));
    }
}
