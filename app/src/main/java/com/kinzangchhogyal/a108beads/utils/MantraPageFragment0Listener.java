package com.kinzangchhogyal.a108beads.utils;

/**
 * Created by kinzang on 28/03/2017.
 */

/*
*Interface to allow PageFragmen0 to communicate with MantraActivity
*Specifically, we need this so that MantraActivity can update the 108 mantra count textview in the fragment
 *  when pressing the reset button which is part of Mantra Activity
 *  Note: couldn't define interface in PageFragment0 class because PageFragment0 already has a interface to communicate
 *  with MantraActivity and we can end up with cyclic definitions
*/
public interface MantraPageFragment0Listener {
    public void updateTxt_mantra_count_108(int i);
}
