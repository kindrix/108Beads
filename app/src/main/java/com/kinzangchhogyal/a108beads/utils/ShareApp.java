package com.kinzangchhogyal.a108beads.utils;

import android.content.Context;
import android.content.Intent;
import com.kinzangchhogyal.a108beads.R;

/**
 * Created by kinzang on 22/03/2017.
 */

//Shares app with other people via a simple text message. Use by menu

public class ShareApp {

    public static void startSharingIntent(Context context){
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        String s = context.getResources().getString(R.string.share_app_message);
        sendIntent.putExtra(Intent.EXTRA_TEXT, s);
        sendIntent.setType("text/plain");
        context.startActivity(sendIntent);
    }
}
