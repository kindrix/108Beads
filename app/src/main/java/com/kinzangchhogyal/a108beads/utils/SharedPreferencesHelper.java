package com.kinzangchhogyal.a108beads.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.kinzangchhogyal.a108beads.R;

import java.util.Date;

/**
 * Created by kinzang on 24/03/2017.
 */

public class SharedPreferencesHelper {
    private static SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;



    public SharedPreferencesHelper(Context context){
        sharedPreferences = context.getSharedPreferences(
                context.getString(R.string.preference_file_key), Context.MODE_PRIVATE
        );
        this.editor = sharedPreferences.edit();
    }

    public void putString(String key, String value){
        editor.putString(key, value);
        editor.commit();
    }

    public void putBoolean(String key, boolean value){
        editor.putBoolean(key, value);
        editor.commit();
    }

    public void putInt(String key, int value){
        editor.putInt(key, value);
        editor.commit();
    }

    public String getString(String key){
        return sharedPreferences.getString(key, "0");
    }

    public boolean getBoolean(String key){
       return sharedPreferences.getBoolean(key, false);
    }

    public int getInt(String key){
        return sharedPreferences.getInt(key, 0);
    }

    //TODO date when app installed




}
