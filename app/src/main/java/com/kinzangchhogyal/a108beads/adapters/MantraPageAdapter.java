package com.kinzangchhogyal.a108beads.adapters;

import android.content.Context;
import android.media.MediaPlayer;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.fragments.MantraPageFragment0;
import com.kinzangchhogyal.a108beads.fragments.MantraPageFragment1;
import com.kinzangchhogyal.a108beads.fragments.MantraPageFragment2;
import com.kinzangchhogyal.a108beads.models.Mantra;

/**
 * Created by kinzang on 14/03/2017.
 */

public class MantraPageAdapter extends FragmentPagerAdapter {

    private String tabTitles[] = new String[3];
    private Mantra mMantra;
    private MediaPlayer mediaPlayer;
    private FragmentManager fm;
    private Context mContext;

    public MantraPageAdapter(FragmentManager fm, Mantra mantra, Context context) {
        super(fm);
        this.mMantra = mantra;
        this.mContext = context;
        tabTitles[0] =  mContext.getString(R.string.tab_1_name);
        tabTitles[1] =  mContext.getString(R.string.tab_2_name);
        tabTitles[2] =  mContext.getString(R.string.tab_3_name);
    }


    //Returns fragment for page. Each page has a particular fragment.
    @Override
    public Fragment getItem(int position) {
        Fragment mantraPageFragment = null;

        switch (position) {

            //Fragment for showing mantra words and keeping track of chants by tapping.
            case 0:  mantraPageFragment = MantraPageFragment0.newInstance(position, mMantra);
                break;

            //Fragment for showing mantra description.
            case 1:  mantraPageFragment = MantraPageFragment1.newInstance(mMantra.getmDesc());
                break;

            ///TODO COMMENT
            case 2:  mantraPageFragment = MantraPageFragment2.newInstance(position);
                break;
            default:break;
        }

        return mantraPageFragment;
    }

    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabTitles[position];
    }

}
