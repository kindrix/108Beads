package com.kinzangchhogyal.a108beads.adapters;

import android.app.Activity;
import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.models.Mantra;
import com.kinzangchhogyal.a108beads.utils.DBHandler;
import com.kinzangchhogyal.a108beads.utils.SharedPreferencesHelper;

import java.text.NumberFormat;
import java.util.ArrayList;

/**
 * Created by kinzang on 7/03/2017.
 * Custom adapter for listview to show list of mantras
 */

public class MantraListAdapter extends BaseAdapter {

    private ArrayList<Mantra> mMantraList;
    private Context mContext;
    private LayoutInflater mInflater;
    SharedPreferencesHelper sharedPreferencesHelper;
    Mantra m;
    NumberFormat numberFormat;
    DBHandler dbHandler;

    //constructor
    public MantraListAdapter(Context mContext, ArrayList<Mantra> mMantraList) {
        this.mMantraList = mMantraList;
        this.mContext = mContext;
        this.mInflater = (LayoutInflater) mContext.getSystemService(Activity.LAYOUT_INFLATER_SERVICE);
        dbHandler = new DBHandler(mContext);
    }

    @Override
    public int getCount() {
        return mMantraList.size();
    }

    @Override
    public Mantra getItem(int i) {
        return mMantraList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    //Method that returns custom view to show list of mantras in Main Activity
    @Override
    public View getView(int i, View convertView, ViewGroup parent) {
        numberFormat = NumberFormat.getInstance();
        sharedPreferencesHelper = new SharedPreferencesHelper(mContext);


        //get custom layout for showing each item of listview
        View v = mInflater.inflate(R.layout.adapter_matra_list, parent, false);

        //get current mantra in arraylist
        m = mMantraList.get(i);

        //check if date has changed and update saved preferences if necessary
        //Check if date has changed since last use
        //If so, reset mantra 108 count
//        if ( sharedPreferencesHelper.getBoolean("has_date_changed")){
//            //ToastHelper.showToastShort(mContext, "Minute has changed");
//            sharedPreferencesHelper.putInt("data_daily_" + m.getmName(), 0);
//            sharedPreferencesHelper.putInt("data_108_count_" + m.getmName(), 0);
//        }


        //show image of buddha
        ImageView img = (ImageView) v.findViewById(R.id.adapter_matra_list_img_buddha);
        int id = this.mContext.getResources().getIdentifier(m.getmImageUri(), "drawable", this.mContext.getPackageName());
        img.setImageResource(id);

        //show details of the mantra counts
        TextView txt_mantra_daily_count = (TextView) v.findViewById(R.id.adapter_mantra_list_txt_daily_count);
        TextView txt_mantra_total_count = (TextView) v.findViewById(R.id.adapter_mantra_list_txt_total_count);

        //Update the following counters incase date has changed
        //IMPORTANT: update mantra instance with daily and total count; we have to do it again here because
        //the listview doesnt' update when we hit the back button in Mantra Activity
        m.setmDailyCount(dbHandler.getMantra(m.getmName()).getmDailyCount());
        m.setmTotalCount(dbHandler.getMantra(m.getmName()).getmTotalCount());
        m.setm108Count(dbHandler.getMantra(m.getmName()).getm108Count());


        //update textviews with daily and total count for mantra
        txt_mantra_daily_count.setText( numberFormat.format(m.getmDailyCount()));
        txt_mantra_total_count.setText(numberFormat.format(m.getmTotalCount()));

        //show sanskrit name
        TextView txt_sanskrit_name = (TextView) v.findViewById(R.id.adapter_mantra_list_txt_sankrit_name);
        txt_sanskrit_name.setText(m.getmName());


        //if phone doesn't support Tibetan unicode, display image for name of Buddha
        TextView txt_dzongkha_name = (TextView) v.findViewById(R.id.adapter_mantra_list_txt_dzongkha_name);
        if(Build.VERSION.SDK_INT < 23) {
            ImageView imgNameDzongkha = (ImageView) v.findViewById(R.id.adapter_mantra_list_img_dzongkha_name);
            int imageID = mContext.getResources().getIdentifier(m.getmImageUri()+"_name_dzongkha", "drawable", mContext.getPackageName() );
            imgNameDzongkha.setImageResource(imageID);
            imgNameDzongkha.setVisibility(View.VISIBLE);
            txt_dzongkha_name.setVisibility(View.GONE);
        } else{
            txt_dzongkha_name.setText(m.getmNameTibetan());
        }
        return v;
    }




}
