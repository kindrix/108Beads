package com.kinzangchhogyal.a108beads.activities;



import android.net.Uri;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.fragments.MantraListFragment;
import com.kinzangchhogyal.a108beads.models.Mantra;
import com.kinzangchhogyal.a108beads.utils.CalendarHelper;
import com.kinzangchhogyal.a108beads.utils.DBHandler;
import com.kinzangchhogyal.a108beads.utils.FileReader;
import com.kinzangchhogyal.a108beads.utils.ShareApp;
import com.kinzangchhogyal.a108beads.utils.SharedPreferencesHelper;
import com.kinzangchhogyal.a108beads.utils.ToastHelper;

import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class MainActivity extends LifeCycleLoggingActivity {
    private SharedPreferencesHelper sharedPreferencesHelper;
    private FileReader fileReader;
    private ArrayList<Mantra> ml = new ArrayList(); //list of mantras
    private DBHandler dbHandler;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //create toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);

        sharedPreferencesHelper = new SharedPreferencesHelper(this);


        //create database
        //if app is installed for first time,
        //open the mantra_data.txt file and create the db

        if (!sharedPreferencesHelper.getBoolean("app_already_installed")){
            //read mantra details from file and add details to array list
            fileReader = new FileReader(this);
            ml = fileReader.getMantraList();

            //create database
            dbHandler = new DBHandler(this);

            //insert records
            for (Mantra m : ml){
                dbHandler.addMantra(m);
            }

        }

        //If you want a logo, uncomment below
        //myToolbar.setLogo(R.mipmap.mantra_logo);

        //Attach MantraListFragment which displays list of mantras. It makes use of  MantraListAdapter.
        FragmentManager fragmentManager =  getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        MantraListFragment fragment = new MantraListFragment();
        fragmentTransaction.add(R.id.activity_main, fragment);
        fragmentTransaction.commit();

        //save installation date and show toast first time app is used
        if(!sharedPreferencesHelper.getBoolean("app_already_installed")) {
            CalendarHelper.setInstallationDate(this, sharedPreferencesHelper);
            ToastHelper.showToastLong(this, getString(R.string.toast_welcome));

            //set vibrate to true for 108
            sharedPreferencesHelper.putBoolean(getString(R.string.prefs_is_set_vibrate_108), true);


        }
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    /* Initialise toolbar menu items */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        //get xml layout for menu
        menuInflater.inflate(R.menu.toolbar_menu, menu);
        return true;
    }

    /* Action handler for menu items */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        //Prepare intent to launch Menu Activity
        Intent intent = new Intent(this, MenuActivity.class);

        //Determine which menu item selected and take appropriate action
        switch (item.getItemId()){

            //Launch share dialogue. This is done by the system.
            case(R.id.menu_share_app):
                ShareApp.startSharingIntent(this);
                break;

            //Launch browser or facebook app. Done by system.
            case(R.id.menu_facebook_us):
                Intent facebookAppIntent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse(getString(R.string.facebook_page)));
                startActivity(facebookAppIntent);
                break;

            //Launch Menu Activity and load it with the How To Chant fragment.
            case(R.id.menu_how_to_chant):
                intent.putExtra("selected_menu_item",  "how_to_chant");
                startActivity(intent);
                break;

            //Launch Menu Activity and load it with the How You Can Help fragment.
            case(R.id.menu_how_you_can_help):
                intent.putExtra("selected_menu_item",  "how_you_can_help");
                startActivity(intent);
                break;

            //Launch Menu Activy and load it with the Settings fragment.
            case(R.id.menu_settings):
                //fragment = new SettingsFragment();
                intent.putExtra("selected_menu_item",  "settings");
                startActivity(intent);
                break;

            //Launch Menu Activy and load it with the About fragment.
            case(R.id.menu_about_app):
                intent.putExtra("selected_menu_item",  "about");
                startActivity(intent);
                break;
            default:
                return  super.onOptionsItemSelected(item);
        }
        return true;
    }

    //For when back button pressed in child toolbar in activity class.
    //Not sure what it does.
    @Override
    public void onBackPressed() {
        //TODO MENU STILL SHOWS UP
        if (getFragmentManager().getBackStackEntryCount() > 0) {
            getFragmentManager().popBackStack();
        } else {
            super.onBackPressed();
        }
    }




}
