package com.kinzangchhogyal.a108beads.activities;

import android.content.Intent;
import android.content.res.Resources;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.os.Vibrator;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.adapters.MantraPageAdapter;
import com.kinzangchhogyal.a108beads.fragments.MantraPageFragment0;
import com.kinzangchhogyal.a108beads.fragments.MantraPageFragment2;
import com.kinzangchhogyal.a108beads.models.Mantra;
import com.kinzangchhogyal.a108beads.utils.CalendarHelper;
import com.kinzangchhogyal.a108beads.utils.ConfirmResetMantraCount;
import com.kinzangchhogyal.a108beads.utils.DBHandler;
import com.kinzangchhogyal.a108beads.utils.MantraPageFragment0Listener;
import com.kinzangchhogyal.a108beads.utils.ResetMantraCountInterface;
import com.kinzangchhogyal.a108beads.utils.SharedPreferencesHelper;

import java.text.NumberFormat;
import java.util.Calendar;

/*
* Mantra Activity is the main activity where the use can recite the mantra and tap the screen to keep
* track of his mantras.
 */

public class MantraActivity extends LifeCycleLoggingActivity implements
        ResetMantraCountInterface, MantraPageFragment0.MantraActivityListener, MantraPageFragment2.MantraActivityListener{

    //one cycle of mantra
    private final static int MANTRA_CYCLE = 108 ;

    Mantra mantra;

    private TextView mTxt_mantra_daily_count;
    private ImageView imageView;

    //these views are not here
    private TextView mTxt_mantra_total_count;
    private TextView mTxt_mantra_108_count;

    //vibration points
    private boolean mIs_set_vibrate_108_chants;
    private boolean mIs_set_vibrate_point_2;
    private int mVibratePoint2;

    //button play audio
    private ImageView mBtn_play_mantra;

    //dbhelper
    DBHandler dbHandler;

    //button reset 108 mantracount
    private ImageView mBtn_reset_mantra_count;


    private MediaPlayer mMediaPlayer;
    private ViewPager viewPager;
    private Vibrator vibrator;
    private NumberFormat numberFormat;

    //for communication with MantraActivity
    private MantraPageFragment0Listener fragmentListener;

    //for getting preferences and  data
    private SharedPreferencesHelper sharedPreferencesHelper;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //set view
        setContentView(R.layout.activity_mantra);

        //read prefernces and saved data
        sharedPreferencesHelper = new SharedPreferencesHelper(this);
        mVibratePoint2 = sharedPreferencesHelper.getInt(getString(R.string.prefs_seekbar_point_2_value));
        mIs_set_vibrate_108_chants = sharedPreferencesHelper.getBoolean(getString(R.string.prefs_is_set_vibrate_108));
        mIs_set_vibrate_point_2 = sharedPreferencesHelper.getBoolean(getString(R.string.prefs_is_set_vibrate_point_2));

        //get details of the mantra from intent initiated by MantraListFragment

        Intent intent = this.getIntent();
        mantra = (Mantra) intent.getParcelableExtra("mantra");

        //numberformat
        numberFormat = NumberFormat.getInstance();

        //get text ui details
        mTxt_mantra_daily_count = (TextView) findViewById(R.id.mantra_activity_txt_mantra_count_daily);

        //initialize view daily count
        update_view_mantra_daily_count();

        //setup dbhandler
        dbHandler = new DBHandler(this);

        //setup media player
        setUpMediaPlayer();

        //set up vibrator
        vibrator = (Vibrator) getSystemService(this.VIBRATOR_SERVICE);



        //set up mantra count reset button
        mBtn_reset_mantra_count = (ImageView) findViewById(R.id.btn_reset_mantra_count);
        mBtn_reset_mantra_count.setOnClickListener(resetMantraCountListener);

        //set image of mantra
        setMantraImage();

        //set up view pager
        viewPager = (ViewPager) findViewById(R.id.viewPager_mantra);
        MantraPageAdapter mpa = new MantraPageAdapter(getSupportFragmentManager(), mantra, this);
        viewPager.setAdapter(mpa);

        Log.d("CurrentViewPage:", viewPager.getCurrentItem()+"" );
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    @Override
    protected void onPause() {
        super.onPause();
        if (mMediaPlayer.isPlaying()){
            mMediaPlayer.pause();
            mBtn_play_mantra.setImageResource(R.drawable.ic_play_circle_outline_white_24dp);
        }

        save_mantra_count();
    }

    //save data on pause
    private void save_mantra_count(){
//        Log.d("COUNT:", mantra.getmDailyCount() +"");
//        Log.d("TOTAL:", mantra.getmTotalCount() +"");
//        sharedPreferencesHelper.putInt("data_108_count_"+mantra.getmName(), mantra.getm108Count());
//        sharedPreferencesHelper.putInt("data_daily_"+mantra.getmName(), mantra.getmDailyCount());
//        sharedPreferencesHelper.putInt("data_total_"+mantra.getmName(), mantra.getmTotalCount());
        dbHandler.updateMantraCount(mantra);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMediaPlayer.release();
    }



    public void update_view_mantra_daily_count(){
        mTxt_mantra_daily_count.setText(numberFormat.format(mantra.getmDailyCount()));
        //mTxt_mantra_daily_count.setText(numberFormat.format(0)+"\nchants today");

    }

    //set up media player
    public void setUpMediaPlayer() {
        try {
            int mediaId = getApplicationContext().getResources().getIdentifier(mantra.getmName().toLowerCase(), "raw", getPackageName());
            mMediaPlayer = MediaPlayer.create(this, mediaId);
        } catch (Resources.NotFoundException e) {
            mMediaPlayer = MediaPlayer.create(this, R.raw.avolokitesvara);
        }
        mMediaPlayer.setLooping(true);
        mBtn_play_mantra = (ImageView) findViewById(R.id.btn_play_mantra);
        mBtn_play_mantra.setOnClickListener(playPauseListener);
    }

    //set mantra image in background
    public void setMantraImage() {
        RelativeLayout ll = (RelativeLayout) findViewById(R.id.layout_mantra);
        imageView = (ImageView) ll.findViewById(R.id.activityMantra_img_mantra);
        int id = getResources().getIdentifier(mantra.getmImageUri(), "drawable", getPackageName());
        imageView.setImageResource(id);
    }


    //
    private View.OnClickListener resetMantraCountListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            if (mantra.getm108Count()>0){
                ConfirmResetMantraCount resetMantraCount = ConfirmResetMantraCount.newInstance(mantra.getm108Count());
                resetMantraCount.show(getSupportFragmentManager(), "resetMantraCount");
            }
        }
    };

    //Listener for play button
    private View.OnClickListener playPauseListener = new View.OnClickListener(){
        @Override
        public void onClick(View view) {
            if(!mMediaPlayer.isPlaying()){
                mMediaPlayer.start();
                mBtn_play_mantra.setImageResource(R.drawable.ic_pause_circle_outline_white_24dp);
            }
            else{
                mMediaPlayer.pause();
                mBtn_play_mantra.setImageResource(R.drawable.ic_play_circle_outline_white_24dp);
            }
        }
    };

    //completion listener
    private MediaPlayer.OnCompletionListener onCompletionListener = new MediaPlayer.OnCompletionListener(){
        @Override
        public void onCompletion(MediaPlayer mediaPlayer) {
            //TODO
        }
    };


    //Reset interface method implementations
    @Override
    public void onChoose() {
        mantra.setmDailyCount(mantra.getmDailyCount() - mantra.getm108Count());
        mantra.setmTotalCount(mantra.getmTotalCount() - mantra.getm108Count());
        update_view_mantra_daily_count();
        mantra.setm108Count(0);
        fragmentListener.updateTxt_mantra_count_108(0);
    }


    public void setFragmentListener(MantraPageFragment0Listener m){
        fragmentListener = m;
    }

    /**********************************************************
    * MantraPageFragment0.MantraActivityListener Interface Methods
     *******************************************************/

    @Override
    public void set_mMantra_108_count(int i) {
        mantra.setm108Count(i);
    }

    @Override
    public int get_mantra_108_count() {
        return mantra.getm108Count();
    }

    /*
    * Increases mantra count; vibrates if it is set to do; resets 108 mantra count to 0 if necessary
     */
    @Override
    public void inc_mTxt_mantra_108_count() {

        mantra.setm108Count(mantra.getm108Count() + 1);
        mantra.setmDailyCount(mantra.getmDailyCount() + 1);
        mantra.setmTotalCount(mantra.getmTotalCount() + 1);

        update_view_mantra_daily_count();

        if (mIs_set_vibrate_point_2 && mantra.getm108Count() % mVibratePoint2 == 0){
            vibrator.vibrate(150);
        }

        if(mantra.getm108Count() == MANTRA_CYCLE && mIs_set_vibrate_108_chants){
                vibrator.vibrate(300);
        }

        if(mantra.getm108Count() > MANTRA_CYCLE){
            mantra.setm108Count(1);
            fragmentListener.updateTxt_mantra_count_108(1);
        }
    }

    @Override
    public ImageView get_btn_reset_mantra_count() {
        return mBtn_reset_mantra_count;
    }

    /**********************************************************
     * MantraPageFragment1.MantraActivityListener Interface Methods
     *******************************************************/

    @Override
    public String get_mantra_name() {
        return mantra.getmName();
    }

    @Override
    public int get_total_mantra_count() {
        return mantra.getmTotalCount();
    }

    @Override
    public int get_chants_per_day() {
        Calendar installDate = CalendarHelper.getInstallationDate(this, sharedPreferencesHelper);
        Calendar dateToday = Calendar.getInstance();
        int numDays =  CalendarHelper.getDiffBetweenTwoCalendars(installDate, dateToday);
        if (numDays == 0) return 0;
        return mantra.getmTotalCount()/numDays;
    }


}
