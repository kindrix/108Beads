package com.kinzangchhogyal.a108beads.activities;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.fragments.AboutFragment;
import com.kinzangchhogyal.a108beads.fragments.HowToChantFragment;
import com.kinzangchhogyal.a108beads.fragments.HowYouCanHelpFragment;
import com.kinzangchhogyal.a108beads.fragments.SettingsFragment;

public class MenuActivity extends LifeCycleLoggingActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);

        //create toolbar
        Toolbar myToolbar = (Toolbar) findViewById(R.id.child_toolbar);
        setSupportActionBar(myToolbar);

        // Get a support ActionBar corresponding to this toolbar
        ActionBar ab = getSupportActionBar();

        // Enable the Up button
        ab.setDisplayHomeAsUpEnabled(true);

        //get calling intent
        Intent intent  = this.getIntent();
        String selected_item = intent.getStringExtra("selected_menu_item");
        Fragment fragment = null;
        //find fragment to display
        switch (selected_item){
            case "how_to_chant":
                fragment = new HowToChantFragment();
                break;
            case "how_you_can_help":
                fragment = new HowYouCanHelpFragment();
                break;
            case "about":
                fragment = new AboutFragment();
                break;
            case "settings":
                fragment = new SettingsFragment();
                break;
            default:break;
        }

        //attach MantraListFragment
        FragmentManager fragmentManager =  getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        fragmentTransaction.add(R.id.activity_menu, fragment);
        fragmentTransaction.commit();



    }
}


