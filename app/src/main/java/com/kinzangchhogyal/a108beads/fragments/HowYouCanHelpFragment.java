package com.kinzangchhogyal.a108beads.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kinzangchhogyal.a108beads.R;

/**
 * Created by kinzang on 25/03/2017.
 */

public class HowYouCanHelpFragment extends  LifeCycleLoggingFragment {
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_how_you_can_help, container, false);
        return view;
    }
}
