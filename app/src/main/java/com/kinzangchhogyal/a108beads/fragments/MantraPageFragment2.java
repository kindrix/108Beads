package com.kinzangchhogyal.a108beads.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.utils.SharedPreferencesHelper;

import java.text.NumberFormat;

/**
 * Created by kinzang on 17/03/2017.
 */

public class MantraPageFragment2 extends LifeCycleLoggingFragment {

    private TextView mTxtChantsPerDay;
    private TextView mTxtCyclesCompeleted;
    private TextView mTxtTotalChants;

    private MantraActivityListener mListener;

    private NumberFormat numberFormat;

    public static MantraPageFragment2 newInstance(int position){
        return new MantraPageFragment2();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) inflater.inflate(R.layout.fragment_page_2, container, false);


        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        numberFormat = NumberFormat.getInstance();

        mListener = (MantraActivityListener) getActivity();
        int total = mListener.get_total_mantra_count();

        //initialise views
        mTxtChantsPerDay = (TextView) getActivity().findViewById(R.id.page2_txt_daily_average);
        mTxtTotalChants = (TextView) getActivity().findViewById(R.id.page2_txt_total);
        mTxtCyclesCompeleted = (TextView) getActivity().findViewById(R.id.page2_txt_num_cycles);

        mTxtTotalChants.setText(numberFormat.format(total));
        mTxtChantsPerDay.setText(numberFormat.format(mListener.get_chants_per_day()));
        mTxtCyclesCompeleted.setText( numberFormat.format((total/108)));
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //Interface to communication with Mantra Activity
    //This is needed as keep track of the mantra count in Mantra Activity and we need to access those variables.
    //We could make them static but could leak to memory leaks especially with static views
    public interface MantraActivityListener {
        public String get_mantra_name();
        public int get_total_mantra_count();
        public int get_chants_per_day();
    }
}
