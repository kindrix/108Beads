package com.kinzangchhogyal.a108beads.fragments;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.kinzangchhogyal.a108beads.R;

/**
 * Created by kinzang on 17/03/2017.
 */

public class MantraPageFragment1 extends LifeCycleLoggingFragment {

    private String mMantraDesc;
    private TextView mView_txt_mantra_Desc;

    public static MantraPageFragment1 newInstance(String mantraDesc){
        //We create a bundle since we cannot pass argument by using a constructor other than a Default constructor.
        //The default constructor is necessary when the frament is recreated by the Android system.
        Bundle bundle = new Bundle();
        bundle.putString("MANTRA_DESC", mantraDesc);
        MantraPageFragment1 m = new MantraPageFragment1();
        m.setArguments(bundle);
        return m;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        //Get Mantra description and save it to the instance variable
        this.mMantraDesc = getArguments().getString("MANTRA_DESC");
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = (View) inflater.inflate(R.layout.fragment_page_1, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mView_txt_mantra_Desc = (TextView) getActivity().findViewById(R.id.fragment_page_1_txt_mantra_desc);
        mView_txt_mantra_Desc.setText(mMantraDesc);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }
}
