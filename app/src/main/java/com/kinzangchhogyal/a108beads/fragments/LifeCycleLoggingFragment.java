package com.kinzangchhogyal.a108beads.fragments;

import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.kinzangchhogyal.a108beads.R;

/**
 * Created by kinzang on 8/03/2017.
 */

public abstract  class LifeCycleLoggingFragment extends Fragment {

    private final String TAG = getClass().getName();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (savedInstanceState != null) {
            // The activity is being re-created. Use the
            // savedInstanceState bundle for initializations either
            // during onCreate or onRestoreInstanceState().
            Log.d(TAG, "onCreate(): fragment re-created");

        } else {
            // Activity is being created anew. No prior saved
            // instance state information available in Bundle object.
            Log.d(TAG, "onCreate(): fragment created anew");
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG, "onCreateView() - the fragment view is about to be created");
        return super.onCreateView(inflater, container, savedInstanceState);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        Log.d(TAG, "onAttach() - the activity is about to be attached");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d(TAG, "onStart() - the activity is about to be started");
    }

    @Override
    public void onPause() {
        super.onPause();
        Log.d(TAG, "onPause() - the activity is about to become paused");
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume() - the activity is about to be resumed");
    }

    @Override
    public void onStop() {
        super.onStop();
        Log.d(TAG, "onStop() - the activity is about to be stopped");
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "onDestroy() - the activity is about to be destroyed");
    }

}
