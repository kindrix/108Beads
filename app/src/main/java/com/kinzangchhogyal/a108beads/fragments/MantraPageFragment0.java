package com.kinzangchhogyal.a108beads.fragments;


import android.animation.Animator;
import android.animation.AnimatorInflater;
import android.animation.AnimatorSet;
import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.activities.MantraActivity;
import com.kinzangchhogyal.a108beads.models.Mantra;
import com.kinzangchhogyal.a108beads.utils.MantraPageFragment0Listener;
import com.kinzangchhogyal.a108beads.utils.SharedPreferencesHelper;

import java.text.NumberFormat;

/**
 * Created by kinzang on 14/03/2017.
 */

public class MantraPageFragment0 extends LifeCycleLoggingFragment implements MantraPageFragment0Listener {

    public static final String PAGE_NUMBER = "PAGE_NUMBER";
    private int mPageID;
    private Mantra mMantra;
    private int mMantraCount;
    private TextView txt_mantra_words;
    private TextView txt_mantra_tibetan;
    ImageView img_mantra_words;
    private TextView txt_mantra_count_108;
    private TextView txt_left_click_area;
    private TextView txt_right_click_area;
    private ImageView img_click_left_area;
    private ImageView img_click_right_area;
    private AnimatorSet animatorSet1;
    private AnimatorSet animatorSet2;
    private AnimatorSet animatorSet3;
    private MantraActivityListener mActivityListener;
    private SharedPreferencesHelper sharedPreferencesHelper;
    private NumberFormat numberFormat;



    //Interface Method MantraPageFrament0Listener
    public void updateTxt_mantra_count_108(int i){
        txt_mantra_count_108.setText(numberFormat.format(i));
    }


    //onclick listener for mantra count
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            mActivityListener.inc_mTxt_mantra_108_count();
            int i = mActivityListener.get_mantra_108_count();
            txt_mantra_count_108.setText( numberFormat.format(i));
            //txt_mantra_count_108.setText( numberFormat.format(i) + "/108");
        }
    };

    //animation end listener for left tap here instructions
    private Animator.AnimatorListener animationEndListenerLeftClickArea = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {
            img_click_left_area.setVisibility(View.VISIBLE);
        }

        @Override
        public void onAnimationEnd(Animator animator) {
            //start animation2 after frist one finishes
            img_click_right_area.setVisibility(View.VISIBLE);
            animatorSet2.start();
        }

        @Override
        public void onAnimationCancel(Animator animator) {}

        @Override
        public void onAnimationRepeat(Animator animator) {}
    };

    //animation end listener for right tap here instructions
    private Animator.AnimatorListener animationEndListenerRightClickArea = new Animator.AnimatorListener() {
        @Override
        public void onAnimationStart(Animator animator) {}

        @Override
        public void onAnimationEnd(Animator animator) {
            //start animation2 after frist one finishes
            img_click_left_area.setVisibility(View.GONE);
            img_click_right_area.setVisibility(View.GONE);

            txt_right_click_area.setVisibility(View.VISIBLE);
            txt_left_click_area.setVisibility(View.VISIBLE);
            //txt_mantra_count_108.setVisibility(View.VISIBLE);
            animatorSet3.start();
        }

        @Override
        public void onAnimationCancel(Animator animator) {}

        @Override
        public void onAnimationRepeat(Animator animator) {}
    };




    //Factory method MantraPageFragment0.
    public static MantraPageFragment0 newInstance(int pageNumber, Mantra mantra){

        //We create a bundle since we cannot pass argument by using a constructor other than a Default constructor.
        //The default constructor is necessary when the frament is recreated by the Android system.
        Bundle bundle = new Bundle();
        bundle.putInt(PAGE_NUMBER, pageNumber);
        bundle.putParcelable("MANTRA",mantra);
        bundle.putInt("COUNT", 0);
        MantraPageFragment0 m = new MantraPageFragment0();
        m.setArguments(bundle);
        return m;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        sharedPreferencesHelper = new SharedPreferencesHelper(getActivity());
        //Save pageID so that proper layout can be used
        mPageID = getArguments().getInt(PAGE_NUMBER);
        mMantra = getArguments().getParcelable("MANTRA");
        mMantraCount = getArguments().getInt("COUNT");
        numberFormat = NumberFormat.getInstance();

        animatorSet1 = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(),
                R.animator.fade_in_fade_out);
        animatorSet1.addListener(animationEndListenerLeftClickArea);
        animatorSet2 = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.fade_in_fade_out);
        animatorSet2.addListener(animationEndListenerRightClickArea);
        animatorSet3 = (AnimatorSet) AnimatorInflater.loadAnimator(getActivity(), R.animator.fade_in);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //get xml layout for page
        //Log.d("Mpage id is:", mPageID+"");
        String xmlFile = "fragment_page_" + mPageID;
        int id = getActivity().getResources().getIdentifier(xmlFile, "layout", getActivity().getPackageName());
        View v = (View) inflater.inflate(id, container, false);
        return v;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {

        txt_mantra_count_108 = (TextView) getActivity().findViewById(R.id.page0_txt_count_108);
        txt_mantra_count_108.setText(numberFormat.format(mActivityListener.get_mantra_108_count()));


        if (mPageID == 0) {

            txt_mantra_words = (TextView) getActivity().findViewById(R.id.page0_txt_mantra_words);
            txt_mantra_words.setText(mMantra.getmWords());

            //check if phone supports Tibetan font
            //Log.d("Mooo Version", Build.VERSION.SDK_INT+"");
            txt_mantra_tibetan = (TextView) getActivity().findViewById(R.id.page0_txt_mantra_tibetan);

            //chnage version to 23 for Carolyn's phone
            if(Build.VERSION.SDK_INT < 23) {
                int mantraImageID = getActivity().getResources().getIdentifier(mMantra.getmImageUri() + "_mantra", "drawable", getActivity().getPackageName());
                img_mantra_words = (ImageView) getActivity().findViewById(R.id.page0_img_mantra_words);
                img_mantra_words.setImageResource(mantraImageID);
                img_mantra_words.setVisibility(View.VISIBLE);
                txt_mantra_tibetan.setVisibility(View.GONE);

            } else{
                txt_mantra_tibetan.setText(mMantra.getmWordsTibetan());
            }
            //TextView i = (TextView) getActivity().findViewById(R.id.txt_left_click_area);
            //i.startAnimation(animFadeIn);
            img_click_left_area = (ImageView) getActivity().findViewById(R.id.page0_img_click_left_area);
            img_click_right_area = (ImageView) getActivity().findViewById(R.id.page0_img_click_right_area);
            img_click_left_area.setOnClickListener(onClickListener);
            img_click_right_area.setOnClickListener(onClickListener);

            //set listeners for tap
            txt_mantra_count_108.setOnClickListener(onClickListener);
            txt_mantra_words.setOnClickListener(onClickListener);
            if(txt_mantra_tibetan.getVisibility() == View.VISIBLE)
                txt_mantra_tibetan.setOnClickListener(onClickListener);
            else
                img_mantra_words.setOnClickListener(onClickListener);
            txt_left_click_area = (TextView) getActivity().findViewById(R.id.page0_txt_left_click_area);
            txt_left_click_area.setOnClickListener(onClickListener);
            txt_right_click_area = (TextView) getActivity().findViewById(R.id.page0_txt_right_click_area);
            txt_right_click_area.setOnClickListener(onClickListener);



            //Set animators to show tap information if app is freshly installed
            if(!sharedPreferencesHelper.getBoolean("hide_click_demo")) {
                animatorSet1.addListener(animationEndListenerLeftClickArea);
                animatorSet2.setTarget(img_click_right_area);
                animatorSet1.setTarget(img_click_left_area);
                animatorSet1.start();
                animatorSet3.setTarget(mActivityListener.get_btn_reset_mantra_count());
                sharedPreferencesHelper.putBoolean("hide_click_demo", true);

            } else{
                img_click_left_area.setVisibility(View.GONE);
                img_click_right_area.setVisibility(View.GONE);
                txt_left_click_area.setVisibility(View.VISIBLE);
                txt_right_click_area.setVisibility(View.VISIBLE);
            }




        }

    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        //register acitivity in fragment
        mActivityListener = (MantraActivityListener) context;
//
        //register fragment in calling activity
        ((MantraActivity) context).setFragmentListener(this);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }


    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    //Interface to communication with Mantra Activity
    //This is needed as keep track of the mantra count in Mantra Activity and we need to access those variables.
    //We could make them static but could leak to memory leaks especially with static views
    public interface MantraActivityListener {
        public void set_mMantra_108_count(int i);
        public int get_mantra_108_count();
        public void inc_mTxt_mantra_108_count();
        public ImageView get_btn_reset_mantra_count();
    }


}
