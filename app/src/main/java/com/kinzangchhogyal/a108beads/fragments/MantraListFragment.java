package com.kinzangchhogyal.a108beads.fragments;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.activities.MantraActivity;
import com.kinzangchhogyal.a108beads.adapters.MantraListAdapter;
import com.kinzangchhogyal.a108beads.models.Mantra;
import com.kinzangchhogyal.a108beads.utils.CalendarHelper;
import com.kinzangchhogyal.a108beads.utils.DBHandler;
import com.kinzangchhogyal.a108beads.utils.FileReader;
import com.kinzangchhogyal.a108beads.utils.SharedPreferencesHelper;
import com.kinzangchhogyal.a108beads.utils.ToastHelper;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;

/**
 * Created by kinzang on 22/03/2017.
 * Display a list of the mantras. The data is read from a text file.
 */

public class MantraListFragment extends LifeCycleLoggingFragment {

    private ArrayList<Mantra> ml = new ArrayList(); //list of mantras
    private SharedPreferencesHelper sharedPreferencesHelper; //for saving data
    private ListView lv; //listview
    private MantraListAdapter ma; //adapter
    private int itemTracker;
    private MyBroadcastReceiver mbr;
    private IntentFilter filter;
    private DBHandler dbHandler;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        sharedPreferencesHelper = new SharedPreferencesHelper(getActivity());

        //broadcast receiver initialise
        mbr = new MyBroadcastReceiver();
        filter = new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION);
        filter.addAction(Intent.ACTION_TIME_TICK);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        //get layout for fragment
        View view = inflater.inflate(R.layout.fragment_mantra_list, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        dbHandler = new DBHandler(getActivity());



        ml = (ArrayList) dbHandler.getAllMantras();

        //attach array list to adapter which take an arraylist of mantras
        ma = new MantraListAdapter(getContext(), ml);

        //create list view and set adapter
        lv = (ListView) getActivity().findViewById(R.id.lst_view);
        lv.setAdapter(ma);

        //list view on click listener
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                //keep track of which item was clicked, NOT USED
                //itemTracker = i;

                //Create new intent that launches MantraActivity showing mantra details
                Intent intent = new Intent(getContext(), MantraActivity.class);

                //pass mantra object and start intent
                Mantra m = ml.get(i);

                intent.putExtra("mantra", m);

                startActivity(intent);
            }
        });
    }

    @Override
    public void onPause() {

        super.onPause();

        //deregister receiver
        getActivity().unregisterReceiver(mbr);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        dbHandler.close();
    }

    @Override
    public void onResume() {
        //Collections.sort(ml);
        //ma.notifyDataSetChanged();
        //register broadcast receiver
        getActivity().registerReceiver(mbr, filter);

        super.onResume();
    }

    //Broadcast receiver checks very minute whether the date changed;
    //Only active when MantraListFragment is onResume.

    private class MyBroadcastReceiver extends BroadcastReceiver {
        @Override
        public void onReceive(Context context, Intent intent) {
            //check if date has changed and update saved preferences if necessary
            //Check if date has changed since last use or while app has been in use
            if ( CalendarHelper.checkIfDateChanged(getActivity(), sharedPreferencesHelper)){
                //show message only if app already installed
                if (sharedPreferencesHelper.getBoolean("app_already_installed"))
                    ToastHelper.showToastLong(getActivity(), getString(R.string.toast_day_has_changed));
                else
                    //set app_already_installed to True
                    sharedPreferencesHelper.putBoolean("app_already_installed", true);
                sharedPreferencesHelper.putBoolean("has_date_changed", true);
                CalendarHelper.setUpdateDayAppLastUsed(getActivity(), sharedPreferencesHelper);
                reset_108_and_daily_count();
            } else{
                sharedPreferencesHelper.putBoolean("has_date_changed", false);
            }
            ma.notifyDataSetChanged();

        }
    }

    //reset 108 count and daily count to 0 if date has changed
    private void reset_108_and_daily_count(){
        for (Mantra m: ml){
            m.setm108Count(0);
            m.setmDailyCount(0);
            dbHandler.updateMantraCount(m);
        }
    }
}
