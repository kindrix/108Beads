package com.kinzangchhogyal.a108beads.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.kinzangchhogyal.a108beads.R;
import com.kinzangchhogyal.a108beads.utils.SharedPreferencesHelper;

/**
 * Created by kinzang on 23/03/2017.
 */

public class SettingsFragment extends LifeCycleLoggingFragment {

    Context context;
    CharSequence text;
    int duration = Toast.LENGTH_SHORT;
    Toast toast;

    CheckBox chk_vibrate_108;
    CheckBox chk_vibrate_point_2;

    TextView txt_vibrate_point_2;

    SeekBar seek_bar;

    SharedPreferencesHelper sharedPreferencesHelper;


    //listener for mantra vibrate point 2 seekbar
    private SeekBar.OnSeekBarChangeListener onSeekBarChangeListener = new SeekBar.OnSeekBarChangeListener() {
        @Override
        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
            txt_vibrate_point_2.setText(i+1+" chants");
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    };



    //to do when checkboxes are checked
    private View.OnClickListener onClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            // Is the view now checked?
            boolean checked = ((CheckBox) view).isChecked();

            // Check which checkbox was clicked
            switch(view.getId()) {
                case R.id.settings_chk_vibrate_108:
                    break;
                case R.id.settings_chk_vibrate_point_2:
                    break;
                default:
                    break;
            }
        }
    };


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        context = getActivity().getApplicationContext();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_settings, container, false);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {


        //get views
        txt_vibrate_point_2 = (TextView) getActivity().findViewById(R.id.settings_txt_vibrate_point_2);
        seek_bar = (SeekBar) getActivity().findViewById(R.id.settings_seek_bar);
        chk_vibrate_108 = (CheckBox) getActivity().findViewById(R.id.settings_chk_vibrate_108);
        chk_vibrate_point_2 = (CheckBox) getActivity().findViewById(R.id.settings_chk_vibrate_point_2);

        //attach listeners
        chk_vibrate_108.setOnClickListener(onClickListener);
        chk_vibrate_point_2.setOnClickListener(onClickListener);
        seek_bar.setOnSeekBarChangeListener(onSeekBarChangeListener);

        //set view values from sharedpreferences

        sharedPreferencesHelper = new SharedPreferencesHelper(getContext());
        chk_vibrate_108.setChecked(sharedPreferencesHelper.getBoolean(getString(R.string.prefs_is_set_vibrate_108)));
        chk_vibrate_point_2.setChecked(sharedPreferencesHelper.getBoolean(getString(R.string.prefs_is_set_vibrate_point_2)));

        //set seekbar value
        seek_bar.setProgress(sharedPreferencesHelper.getInt( getString(R.string.prefs_seekbar_point_2_value)) - 1);


    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        sharedPreferencesHelper.putBoolean(getString(R.string.prefs_is_set_vibrate_108), chk_vibrate_108.isChecked());
        sharedPreferencesHelper.putBoolean(getString(R.string.prefs_is_set_vibrate_point_2), chk_vibrate_point_2.isChecked());
        sharedPreferencesHelper.putInt(getString(R.string.prefs_seekbar_point_2_value), seek_bar.getProgress()+1);
    }

}
