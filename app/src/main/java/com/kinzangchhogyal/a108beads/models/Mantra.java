package com.kinzangchhogyal.a108beads.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Comparator;

/**
 * Created by kinzang on 7/03/2017.
 */

public class Mantra implements Parcelable, Comparable<Mantra> {

    private String mName; //mantra name
    private String mNameTibetan; //Tibetan Unicode name
    private String mWords; //mantra words
    private String mWordsTibetan; //Tibetan unicode mantra words
    private String mDesc; //mantra desc
    private String mImageUri; //image resource for detail view of mantra
    private int mDailyCount;
    private int mTotalCount;
    private int m108Count;

    //Default Contructor
    public Mantra(){}

    //Constructor with params
    public Mantra(String mName, String mNameTibetan, String mWords, String mWordsTibetan, String mImageUri, String mDesc,
                  int m108Count, int mDailyCount, int mTotalCount) {
        this.mName = mName;
        this.mNameTibetan = mNameTibetan;
        this.mWords = mWords;
        this.mWordsTibetan = mWordsTibetan;
        this.mImageUri = mImageUri; //image file is save as name but all lowercase
        this.mDesc = mDesc;
        this.m108Count = m108Count;
        this.mDailyCount = mDailyCount;
        this.mTotalCount = mTotalCount;
    }

    //Constructor for making Mantra parceleable
    public Mantra(Parcel in){
        mName = in.readString();
        mNameTibetan = in.readString();
        mWords = in.readString();
        mWordsTibetan = in.readString();
        mImageUri = in.readString();
        mDesc = in.readString();
        m108Count = in.readInt();
        mDailyCount = in.readInt();
        mTotalCount = in.readInt();

    }

    //Getter and Setters

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmNameTibetan() {
        return mNameTibetan;
    }

    public void setmNameTibetan(String mNameTibetan) {
        this.mNameTibetan = mNameTibetan;
    }

    public String getmWords() {
        return mWords;
    }

    public void setmWords(String mWords) {
        this.mWords = mWords;
    }

    public String getmWordsTibetan() {
        return mWordsTibetan;
    }

    public void setmWordsTibetan(String mWordsTibetan) {
        this.mWordsTibetan = mWordsTibetan;
    }

    public String getmDesc() {
        return mDesc;
    }

    public void setmDesc(String mDesc) {
        this.mDesc = mDesc;
    }

    public String getmImageUri() {
        return mImageUri;
    }

    public void setmImageUri(String mImageUri) {
        this.mImageUri = mImageUri;
    }

    public int getmDailyCount() {
        return mDailyCount;
    }

    public void setmDailyCount(int mDailyCount) {
        this.mDailyCount = mDailyCount;
    }

    public int getmTotalCount() {
        return mTotalCount;
    }

    public void setmTotalCount(int mTotalCount) {
        this.mTotalCount = mTotalCount;
    }

    public int getm108Count() {
        return m108Count;
    }

    public void setm108Count(int m108Count) {
        this.m108Count = m108Count;
    }

    //Methods to make Mantra object parcelable
    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(mName);
        parcel.writeString(mNameTibetan);
        parcel.writeString(mWords);
        parcel.writeString(mWordsTibetan);
        parcel.writeString(mImageUri);
        parcel.writeString(mDesc);
        parcel.writeInt(m108Count);
        parcel.writeInt(mDailyCount);
        parcel.writeInt(mTotalCount);
    }

    public static final Parcelable.Creator<Mantra> CREATOR
            = new Parcelable.Creator<Mantra>() {

        // This simply calls our new constructor (typically private) and
        // passes along the unmarshalled `Parcel`, and then returns the new object!
        @Override
        public Mantra createFromParcel(Parcel in) {
            return new Mantra(in);
        }

        // We just need to copy this and change the type to match our class.
        @Override
        public Mantra[] newArray(int size) {
            return new Mantra[size];
        }
    };




    @Override
    public int compareTo(Mantra mantra) {
        return  mantra.getmDailyCount() - this.getmDailyCount();
    }
}
