Padmasambhava,གུ་རུ་རིན་པ་ོཆ་ེ,Om Ah Hum Vajra Guru Padma Siddhi Hum,ཨོཾ་ཨཿཧཱུྂ་བཛྲ་གུ་རུ་པདྨ་སིདྡྷི་ཧཱུྂ༔,padmasambhava
Avalokitesvara,སྤྱན་རས་གཟིགས་,Om mani padme hum,ཨོཾ་མ་ཎི་པདྨེ་ཧཱུྂ༔,avalokitesvara
Amitabha,འོད་དཔག་མེད་,Om Amidewa Hrih,ༀ་ཨ་མ་ིདེ་ཝ་ཧཷ༔,amitabha
Tara,སྒྲོལ་མ་,Om Tare Tuttare Ture Svaha,ཨ་ོཾཏཱ་ར་ེཏུཏྟཱ་ར་ེཏུ་ར་ེསྭཱཧཱ༔,tara
Bhaisajyaguru,སངས་རྒྱས་སྨན་བླ་,Om Bekandze Bekandze Maha Bekandze Ra Dza Samugate Svaha,ༀ་བྷེ་ཥ་ཛྱེ་བྷེ་ཥ་ཛྱེ་མཧཱ་བྷེ་ཥ་ཛྱེ་རཱ་ཛཱ་ས་མུངྒ་ཏེ་སྭཱཧཱ༔,bhaisajyaguru
Manjushri,འཇམ་དཔལ་དབྱངས་,Om Arapacana dhih, ༀ་ཨ་ར་པ་ཙ་ན་དྷཱི༔,manjushri
Vajrapani,ཕྱག་ན་རྡོ་རྗེ་,Om Vajrapani Hum,ཨོཾ་བཛྲ་པཱ་ཎི་ཧཱུྂ༔,vajrapani
Amitayus,ཚེ་དཔག་མེད་,Om Ama Ra Ni Dzi Wat Ti Ye Svaha,ༀ་ཨཱ་མ་ར་ནི་ཛི་ཝན་ཏེ་ཡེ་སྭཱ་ཧཱ༔,amitayus